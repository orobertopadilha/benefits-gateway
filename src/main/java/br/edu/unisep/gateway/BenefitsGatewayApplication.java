package br.edu.unisep.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BenefitsGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(BenefitsGatewayApplication.class, args);
    }

}
