package br.edu.unisep.gateway.security.filter;

import br.edu.unisep.gateway.security.utils.JwtUtils;
import lombok.AllArgsConstructor;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

@RefreshScope
@Component
@AllArgsConstructor
public class GatewaySecurityFilter implements GlobalFilter {

    private final JwtUtils jwtUtils;

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {

        if (requiresAuthorization(exchange)) {
            return handleAuthorization(exchange, chain);
        }

        return chain.filter(exchange);
    }

    private Mono<Void> handleAuthorization(ServerWebExchange exchange, GatewayFilterChain chain) {
        var headers = exchange.getRequest().getHeaders();

        if (headers.containsKey("Authorization")) {
            var token = headers.get("Authorization").get(0);

            if (jwtUtils.isValid(token)) {
                var claims = jwtUtils.getClaims(token);
                var userId = claims.getSubject();

                exchange.getRequest()
                        .mutate()
                        .header("auth-user-id", userId);

                return chain.filter(exchange);
            }
        }

        return handleAuthError(exchange.getResponse());
    }

    private Mono<Void> handleAuthError(ServerHttpResponse response) {
        response.setStatusCode(HttpStatus.UNAUTHORIZED);
        return response.setComplete();
    }

    private boolean requiresAuthorization(ServerWebExchange exchange) {
        var request = exchange.getRequest();
        return !request.getPath().toString().startsWith("/benefits-users-api");
    }

}
